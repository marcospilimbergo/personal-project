# Beautiful Forms

This is a light-weight and super fast React form. This was made as a prototype to enter the form market. Our first form was built strictly for the Health Insurance Industry.

To install: 
npm install

To run:
npm start
