import React, {Component} from 'react';
import {Button, ButtonGroup, Col, CustomInput, Form, FormGroup, Input, Label, Row} from "reactstrap";
import {Step, Steps, Wizard} from 'react-albus';
import Container from "reactstrap/es/Container";
import {postFormData} from "../../../state/actions/formActions";
import InputMask from 'react-input-mask';
import {AvField, AvForm} from 'availity-reactstrap-validation';
import QueryString from 'query-string';
import Slider from 'rc-slider';
import './HealthInsuranceForm.css'
import {connect} from "react-redux";
import {Line} from "rc-progress";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import AddressAutocomplete from "../../ui/AddressAutocomplete/AddressAutocomplete";

class HealthInsuranceForm extends Component {


    constructor(props) {
        super(props);
        this.state = {
            householdIncome: '$50,000 or less',
            householdIncomeValue: '',
            majorLifeEvents: '',
            errors: {},
            hasMajorLifeEvents: false,
            dateOfBirthDay: '',
            dateOfBirthMonth: '',
            dateOfBirthYear: '',
            firstName: '',
            lastName: '',
            email: '',
            phoneNumber: '',
            address: '',
            zipCode: '',
        }
    }

    componentDidMount() {
        const queryParams = QueryString.parse(this.props.router.location.search);

        const zipCode = queryParams.zipCode;
        const segmentType = queryParams.segmentType;
        const leadspediaRequestId = queryParams.lpRequestId;

        this.setState({zipCode, segmentType, leadspediaRequestId});
    }

    /**
     *
     * @param event
     * @param next
     * @returns {Promise<void>}
     */
    onChange = (event, next, stepId) => {

        const {target} = event;
        const {name} = target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        console.log(event.type);

        if (name === 'majorLifeEvents') {

            if (value === 'None of these apply') {
                this.setState({
                    [name]: value
                }, () => {
                    next();
                })
            } else {
                this.setState({
                    [name]: this.state.majorLifeEvents.includes(value) ?
                        (this.state.majorLifeEvents.includes(value + ", ") ?
                            this.state.majorLifeEvents.replace(value + ', ', '') :
                            this.state.majorLifeEvents.replace(value, '')) :
                        (this.state.majorLifeEvents ?
                            this.state.majorLifeEvents + ', ' + value :
                            value)
                })
            }
        } else if (name === 'isPregnant' || name === 'treatedForAilments') {
            console.log('2');
            this.setState({
                [name]: value,
            }, function () {
                next();
            });
        } else if (name === 'dateOfBirthDay' || name === 'dateOfBirthMonth' || name === 'dateOfBirthYear') {
            console.log('3');
            this.setState({
                [name]: value,
            }, () => {
                if (this.state.dateOfBirthDay && this.state.dateOfBirthMonth && this.state.dateOfBirthYear) {
                    const dateOfBirth = this.state.dateOfBirthMonth + "/" + this.state.dateOfBirthDay + "/" + this.state.dateOfBirthYear;
                    this.setState({
                        dateOfBirth,
                    });
                }
            });

        } else if (name === 'hasMajorLifeEvents') {
            console.log('4');

            if (value === 'No') {
                this.setState({
                    majorLifeEvents: value
                }, () => {
                    next();
                })
            }

            this.setState({hasMajorLifeEvents: value === 'Yes'});

        } else if (event.type === "click") {
            console.log('4');
            console.log('name: ' + name + 'value: ' + value);
            this.setState({
                [name]: value,
            }, function () {
                next();
            });
        } else {
            console.log('5');
            this.setState({
                [name]: value,
            });
        }
    };

    /**
     *
     * @param emailOrPhoneNumber
     * @returns {boolean}
     */
    hasValidEmailAndPhoneNumber(emailOrPhoneNumber) {
        const emailTest = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/
        const {email} = this.state;

        if (emailOrPhoneNumber === 'email') {
            if (emailTest.test(email.toString().toLocaleLowerCase())) {
                return true;
            }
        }

        return false;
    }

    /**
     *
     * @param e
     */
    onSubmit = (e, stepId) => {

        this.onStep(null, stepId, null);

        if (!this.state.firstName || !this.state.lastName || !this.state.email || !this.state.phoneNumber) {
            return;
        } else {
            e.preventDefault();

            let postData = {
                firstName: this.state.firstName,
                lastName: this.state.lastName,
                phoneNumber: this.state.phoneNumber,
                email: this.state.email,
                dateOfBirth: this.state.dateOfBirth,
                householdIncome: this.state.householdIncome,
                householdSize: this.state.householdSize,
                gender: this.state.gender,
                majorLifeEvents: this.state.majorLifeEvents,
                treatedForAilments: this.state.treatedForAilments,
                isPregnant: this.state.isPregnant,
                address: this.state.address,
                zipCode: this.state.zipCode,
                segmentType: this.state.segmentType,
                leadspediaRequestId: this.state.leadspediaRequestId
            };

            this.props.postForm(postData);
        }
    };

    /**
     *
     * @param stepId
     * @returns {*}
     */
    getErrorsForStep = (stepId) => {
        let errors = [];

        switch (stepId) {
            case 'q0':
                if (!this.state.treatedForAilments) errors.push("Please select either 'Yes' or 'No' ");
                return errors;
            case 'q1':
                if (!this.state.isPregnant) errors.push("Please select either 'Yes' or 'No' ");
                return errors;
            case 'q2':
                if (!this.state.majorLifeEvents) errors.push("Please select one or multiple answers");
                return errors;
            case 'q3':
                if (!this.state.dateOfBirth) errors.push("Please fill in your date of birth");
                return errors;
            case 'q4':
                if (!this.state.householdIncome) errors.push("Please select a range");
                return errors;
            case 'q5':
                if (!this.state.householdSize) errors.push("Please select one of the options above");
                return errors;
            case 'q6':
                if (!this.state.gender) errors.push("Please 'male' or 'female' ");
                return errors;
            case 'q7':

                if (!this.state.firstName) {
                    errors.push("Please enter your first name")
                }

                if (!this.state.lastName) {
                    errors.push("Please enter your last name")
                }

                const email = 'email';
                if (!this.state.phoneNumber) {
                    errors.push("Please enter your email")
                }

                const phoneNumber = 'phoneNumber';
                if (!this.state.email) {
                    errors.push("Please enter your phone number")
                }
                return errors;
            default:
                return null;
        }
    };

    /**
     *
     * @param stepId
     * @returns {*}
     */
    onValidate = (stepId) => {
        let errors = this.getErrorsForStep(stepId);
        let errorString = "";
        if (errors && errors.length > 0) errors.forEach((error, idx) => idx !== errors.length ? errorString += error : errorString += (error + ", "));
        return errorString || "";
    };

    /**
     *
     */
    onStep = (nextOrPrev, stepId, type) => {

        console.log('type: ' + nextOrPrev, 'step: ' + stepId, 'typeName: ' + type);

        if (type === 'prev') {
            nextOrPrev();
        } else {
            const errors = this.onValidate(stepId);
            let stateErrors = this.state.errors;
            stateErrors[stepId] = errors;
            this.setState({errors: stateErrors});

            if (type === 'next') {

                if (stateErrors[stepId].length === 0) {
                    nextOrPrev();
                }
            }
        }
    };

    /**
     *
     */
    getHouseholdIncomeTiers = (tier) => {
        switch (tier) {
            case 0:
                return "$50,000 or less";
            case 1:
                return "$51,000 to $99,000";
            case 2:
                return "$100,000 to $149,000";
            case 3:
                return "$150,000 to $249,000";
            case 4:
                return "$250,000 or more";
            default:
                break;
        }
    };

    /**
     *
     */
    onSlide = (e, slider) => {
        const obj = {};
        console.log('e' + e);
        obj[slider] = this.getHouseholdIncomeTiers(e);
        obj['householdIncomeValue'] = e;
        console.log('obj', obj);
        this.setState(obj);
    };


    render() {
        const marks = {
            0: '$50,000 or less',
            1: '$51,000 to $99,000',
            2: '$100,000 to $149,000',
            3: '$150,000 to $249,000',
            4: '$250,000 or more'
        };

        return (
            <Container>
                <Row className="health-insurance-form">
                    <Col>
                        <Wizard
                            render={({step, steps}) => (
                                <div className="form-content">
                                    <Line
                                        trailWidth={0}
                                        strokeColor={'#5993f2'}
                                        percent={((steps.indexOf(step) + 1) / steps.length * 100)}
                                        className="progress-bar"
                                    />
                                    <div>
                                        <p className={'progress-number'}>{((steps.indexOf(step) + 1) / steps.length * 100).toFixed(0)}%</p>
                                    </div>
                                    <Form className={'userForm'} onSubmit={this.onSubmit}>
                                        <Steps key={step.id} step={step}>
                                            <Step
                                                id="q0"
                                                render={({next}) => (
                                                    <Container>
                                                        <Row>
                                                            <Col sm={'12'}>
                                                                <span>
                                                                <h2 className={'card-text'}>Hello! Have you had: cancer, heart attack, stroke, diabetes, AIDS/HIV, or pulmonar disease in the past 5 years?</h2>
                                                                </span>
                                                            </Col>
                                                        </Row>
                                                        <Row>
                                                            <p className={'suggestion-text'}>Select Yes if you have
                                                                been
                                                                treated for ONE or more.
                                                            </p>
                                                        </Row>
                                                        <Row>
                                                            <Col md="12">
                                                                <div className={"btn-group"}>
                                                                    <Button
                                                                        className={this.state.treatedForAilments === 'No' ? 'answerButtonActive' : 'answerButton'}
                                                                        type={'Button'}
                                                                        name={'treatedForAilments'}
                                                                        value={'No'}
                                                                        onClick={(e) => this.onChange(e, next, step.id)}
                                                                    > No
                                                                    </Button>
                                                                    <Button
                                                                        className={this.state.treatedForAilments === 'Yes' ? 'answerButtonActive' : 'answerButton'}
                                                                        type={'Button'}
                                                                        name={'treatedForAilments'}
                                                                        value={'Yes'}
                                                                        onClick={(e) => this.onChange(e, next)}
                                                                    > Yes
                                                                    </Button>
                                                                </div>
                                                            </Col>
                                                        </Row>
                                                        <Row>
                                                            {this.state.errors[step.id] && !this.state.treatedForAilments &&
                                                            <span className={'errors'}>Please select either 'Yes' or 'No' </span>
                                                            }
                                                        </Row>
                                                        <Row>
                                                            <Col xs="12">
                                                                <div
                                                                    className={"btn-group"}>
                                                                    <Button className={'page-button-solo'}
                                                                            onClick={() => this.onStep(next, step.id, 'next')}>Next
                                                                    </Button>
                                                                </div>
                                                            </Col>
                                                        </Row>
                                                    </Container>
                                                )}/>
                                            <Step
                                                id="q1"
                                                render={({next, previous}) => (
                                                    <div>
                                                        <Row>
                                                            <div className={'container h-100'}>
                                                                <div
                                                                    className={'d-flex align-items-center justify-content-center h-100'}>
                                                                    <div className={'d-flex flex-column'}>
                                                                                <span>
                                                                                   <h2 className={'card-text'}>Is anyone on this policy pregnant?</h2>
                                                                                </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </Row>
                                                        <Row>
                                                        </Row>
                                                        <Row>
                                                            <Col>
                                                                <div className={"btn-group"}>
                                                                    <Button
                                                                        className={this.state.isPregnant === 'No' ? 'answerButtonActive' : 'answerButton'}
                                                                        type="Button"
                                                                        name={'isPregnant'}
                                                                        value={'No'}
                                                                        onClick={(e) => this.onChange(e, next)}
                                                                    > No
                                                                    </Button>
                                                                    <Button
                                                                        className={this.state.isPregnant === 'Yes' ? 'answerButtonActive' : 'answerButton'}
                                                                        type="Button"
                                                                        name={'isPregnant'}
                                                                        value={'Yes'}
                                                                        onClick={(e) => this.onChange(e, next)}
                                                                    > Yes
                                                                    </Button>
                                                                </div>
                                                            </Col>
                                                        </Row>
                                                        <Row>
                                                            {this.state.errors[step.id] && !this.state.isPregnant &&
                                                            <span className={'errors'}>Please select either 'Yes' or 'No' </span>
                                                            }
                                                        </Row>
                                                        <Row>
                                                            <Col>
                                                                <div
                                                                    className={'d-flex align-items-center justify-content-center h-100'}>
                                                                    <div className={'d-flex flex-column'}>
                                                                        <Button className={'page-button-back'}
                                                                                onClick={() => this.onStep(previous, step.id, 'prev')}>Back
                                                                        </Button>
                                                                    </div>
                                                                    <div className={'d-flex flex-column'}>
                                                                        <Button className={'page-button'}
                                                                                onClick={() => this.onStep(next, step.id, 'next')}>Next
                                                                        </Button>
                                                                    </div>
                                                                </div>
                                                            </Col>
                                                        </Row>
                                                    </div>
                                                )}
                                            />
                                            <Step
                                                id="q2"
                                                render={({next, previous}) => (
                                                    <div>
                                                        <Row>
                                                            <div className={'container h-100'}>
                                                                <div
                                                                    className={'d-flex align-items-center justify-content-center h-100'}>
                                                                    <div className={'d-flex flex-column'}>
                                                                                <span>
                                                                                   <h2 className={'card-text'}>Have you experienced any major life events?</h2>
                                                                                </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </Row>
                                                        <Row>
                                                            <p className={'suggestion-text'}>Please select all that
                                                                apply.</p>
                                                        </Row>
                                                        <Row>
                                                            <Col>
                                                                <div className={'btn-group'}>
                                                                    <Button value={'No'}
                                                                            name={'hasMajorLifeEvents'}
                                                                            type={'Button'}
                                                                            className={this.state.majorLifeEvents === 'No' ? 'answerButtonActive' : 'answerButton'}
                                                                            onClick={(e) => this.onChange(e, next)}
                                                                    >No
                                                                    </Button>
                                                                    <Button value={'Yes'}
                                                                            name={'hasMajorLifeEvents'}
                                                                            type={'Button'}
                                                                            className={this.state.hasMajorLifeEvents ? 'answerButtonActive' : 'answerButton'}
                                                                            onClick={(e) => this.onChange(e, next)}
                                                                    > Yes
                                                                    </Button>
                                                                </div>
                                                            </Col>
                                                        </Row>

                                                        {this.state.hasMajorLifeEvents &&
                                                        <div>
                                                            <Row>
                                                                <Col>
                                                                    <div className={'btn-group'}>
                                                                        <Button value={'None of these apply'}
                                                                                name={'majorLifeEvents'}
                                                                                type={'Button'}
                                                                                className={this.state.majorLifeEvents === 'None of these apply' ? 'answerButtonActive' : 'answerButton'}
                                                                                onClick={(e) => this.onChange(e, next)}
                                                                        > None of these apply
                                                                        </Button>
                                                                        <Button value={'Lost Health Insurance Coverage'}
                                                                                name={'majorLifeEvents'}
                                                                                type={'Button'}
                                                                                className={this.state.majorLifeEvents.includes('Lost Health Insurance Coverage') ? 'answerButtonActive' : 'answerButton'}
                                                                                onClick={(e) => this.onChange(e, next)}
                                                                        > Lost Health Insurance Coverage
                                                                        </Button>
                                                                    </div>
                                                                </Col>
                                                            </Row>
                                                            <Row>
                                                                <Col>
                                                                    <ButtonGroup>
                                                                        <Button value={'Gotten Married or Divorced'}
                                                                                name={'majorLifeEvents'}
                                                                                type={'Button'}
                                                                                className={this.state.majorLifeEvents.includes('Gotten Married or Divorced') ? 'answerButtonActive' : 'answerButton'}
                                                                                onClick={(e) => this.onChange(e, next)}
                                                                        > Gotten Married or Divorced
                                                                        </Button>
                                                                        <Button value={'Had a baby or had a child'}
                                                                                name={'majorLifeEvents'}
                                                                                type={'Button'}
                                                                                className={this.state.majorLifeEvents.includes('Had a baby or had a child') ? 'answerButtonActive' : 'answerButton'}
                                                                                onClick={(e) => this.onChange(e, next)}
                                                                        > Had a baby or a child
                                                                        </Button>
                                                                    </ButtonGroup>
                                                                </Col>
                                                            </Row>
                                                            <Row>
                                                                <Col>
                                                                    <div className={'btn-group'}>
                                                                        <Button value={'Lost a parent or spouse'}
                                                                                name={'majorLifeEvents'}
                                                                                type={'Button'}
                                                                                className={this.state.majorLifeEvents.includes('Lost a parent or spouse') ? 'answerButtonActive' : 'answerButton'}
                                                                                onClick={(e) => this.onChange(e, next)}
                                                                        > Lost a parent or spouse
                                                                        </Button>
                                                                        <Button value={'Moved to a new state'}
                                                                                name={'majorLifeEvents'}
                                                                                type={'Button'}
                                                                                className={this.state.majorLifeEvents.includes('Move to a new state') ? 'answerButtonActive' : 'answerButton'}
                                                                                onClick={(e) => this.onChange(e, next)}
                                                                        > Move to a new state
                                                                        </Button>
                                                                    </div>
                                                                </Col>
                                                            </Row>
                                                            <Row>
                                                                <Col>
                                                                    <div className={'btn-group'}>
                                                                        <Button value={'Lost your job'}
                                                                                name={'majorLifeEvents'}
                                                                                type={'Button'}
                                                                                className={this.state.majorLifeEvents.includes('Lost your job') ? 'answerButtonActive' : 'answerButton'}
                                                                                onClick={(e) => this.onChange(e, next)}
                                                                        > Lost your job
                                                                        </Button>
                                                                        <Button value={'Started a new job'}
                                                                                name={'majorLifeEvents'}
                                                                                type={'Button'}
                                                                                className={this.state.majorLifeEvents.includes('Started a new job') ? 'answerButtonActive' : 'answerButton'}
                                                                                onClick={(e) => this.onChange(e, next)}
                                                                        > Started a new job
                                                                        </Button>
                                                                    </div>
                                                                </Col>
                                                            </Row>
                                                        </div>
                                                        }
                                                        <Row>
                                                            {this.state.errors[step.id] && !this.state.majorLifeEvents &&
                                                            <span className={'errors'}>Please select one or multiple answers </span>
                                                            }
                                                        </Row>
                                                        <Row>
                                                            <Col>
                                                                {/*<div className={'container h-100'}>*/}
                                                                <div
                                                                    className={'d-flex align-items-center justify-content-center h-100'}>
                                                                    <div className={'d-flex flex-column'}>
                                                                        <Button className={'page-button-back'}
                                                                                onClick={() => this.onStep(previous, step.id, 'prev')}>Back
                                                                        </Button>
                                                                    </div>
                                                                    <div className={'d-flex flex-column'}>
                                                                        <Button className={'page-button'}
                                                                                onClick={() => this.onStep(next, step.id, 'next')}>Next
                                                                        </Button>
                                                                    </div>
                                                                </div>
                                                                {/*</div>*/}
                                                            </Col>
                                                        </Row>
                                                    </div>
                                                )}
                                            />
                                            <Step
                                                id="q3"
                                                render={({next, previous}) => (
                                                    <div>
                                                        <Row>
                                                            <div className={'container h-100'}>
                                                                <div
                                                                    className={'d-flex align-items-center justify-content-center h-100'}>
                                                                    <div className={'d-flex flex-column'}>
                                                                        <span>
                                                                            <h2 className={'card-text'}>Date of birth</h2>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </Row>
                                                        <Row>
                                                        </Row>
                                                        <Row>
                                                            <Col md={4} xs={12}>
                                                                <FormGroup>
                                                                    <Label>Month</Label>
                                                                    <CustomInput type={'select'}
                                                                                 name={'dateOfBirthMonth'}
                                                                                 value={this.state.dateOfBirthMonth}
                                                                                 onChange={(e) => this.onChange(e, null)}>
                                                                        <option value="">MM</option>
                                                                        <option value="01">01</option>
                                                                        <option value="02">02</option>
                                                                        <option value="03">03</option>
                                                                        <option value="04">04</option>
                                                                        <option value="05">05</option>
                                                                        <option value="06">06</option>
                                                                        <option value="07">07</option>
                                                                        <option value="08">08</option>
                                                                        <option value="09">09</option>
                                                                        <option value="10">10</option>
                                                                        <option value="11">11</option>
                                                                        <option value="12">12</option>
                                                                    </CustomInput>
                                                                </FormGroup>
                                                            </Col>
                                                            <Col md={4} xs={12}>
                                                                <FormGroup>
                                                                    <Label>Day</Label>
                                                                    <CustomInput type={'select'}
                                                                                 name={'dateOfBirthDay'}
                                                                                 value={this.state.dateOfBirthDay}
                                                                                 onChange={(e) => this.onChange(e, null)}>
                                                                        <option value="Day">DD</option>
                                                                        <option value="01">1</option>
                                                                        <option value="02">2</option>
                                                                        <option value="03">3</option>
                                                                        <option value="04">4</option>
                                                                        <option value="05">5</option>
                                                                        <option value="06">6</option>
                                                                        <option value="07">7</option>
                                                                        <option value="08">8</option>
                                                                        <option value="09">9</option>
                                                                        <option value="10">10</option>
                                                                        <option value="11">11</option>
                                                                        <option value="12">12</option>
                                                                        <option value="13">13</option>
                                                                        <option value="14">14</option>
                                                                        <option value="15">15</option>
                                                                        <option value="16">16</option>
                                                                        <option value="17">17</option>
                                                                        <option value="18">18</option>
                                                                        <option value="19">19</option>
                                                                        <option value="20">20</option>
                                                                        <option value="21">21</option>
                                                                        <option value="22">22</option>
                                                                        <option value="23">23</option>
                                                                        <option value="24">24</option>
                                                                        <option value="25">25</option>
                                                                        <option value="26">26</option>
                                                                        <option value="27">27</option>
                                                                        <option value="28">28</option>
                                                                        <option value="29">29</option>
                                                                        <option value="30">30</option>
                                                                        <option value="31">31</option>
                                                                    </CustomInput>
                                                                </FormGroup>
                                                            </Col>
                                                            <Col md={4} xs={12}>
                                                                <FormGroup>
                                                                    <Label>Year</Label>
                                                                    <CustomInput type={'select'}
                                                                                 name={'dateOfBirthYear'}
                                                                                 value={this.state.dateOfBirthYear}
                                                                                 onChange={(e) => this.onChange(e, null)}>
                                                                        <option value="Year">YYYY</option>
                                                                        <option value="2019">2019</option>
                                                                        <option value="2018">2018</option>
                                                                        <option value="2017">2017</option>
                                                                        <option value="2016">2016</option>
                                                                        <option value="2015">2015</option>
                                                                        <option value="2014">2014</option>
                                                                        <option value="2013">2013</option>
                                                                        <option value="2012">2012</option>
                                                                        <option value="2011">2011</option>
                                                                        <option value="2010">2010</option>
                                                                        <option value="2009">2009</option>
                                                                        <option value="2008">2008</option>
                                                                        <option value="2007">2007</option>
                                                                        <option value="2006">2006</option>
                                                                        <option value="2005">2005</option>
                                                                        <option value="2004">2004</option>
                                                                        <option value="2003">2003</option>
                                                                        <option value="2002">2002</option>
                                                                        <option value="2001">2001</option>
                                                                        <option value="2000">2000</option>
                                                                        <option value="1999">1999</option>
                                                                        <option value="1998">1998</option>
                                                                        <option value="1997">1997</option>
                                                                        <option value="1996">1996</option>
                                                                        <option value="1995">1995</option>
                                                                        <option value="1994">1994</option>
                                                                        <option value="1993">1993</option>
                                                                        <option value="1992">1992</option>
                                                                        <option value="1991">1991</option>
                                                                        <option value="1990">1990</option>
                                                                        <option value="1989">1989</option>
                                                                        <option value="1988">1988</option>
                                                                        <option value="1987">1987</option>
                                                                        <option value="1986">1986</option>
                                                                        <option value="1985">1985</option>
                                                                        <option value="1984">1984</option>
                                                                        <option value="1983">1983</option>
                                                                        <option value="1982">1982</option>
                                                                        <option value="1981">1981</option>
                                                                        <option value="1980">1980</option>
                                                                        <option value="1979">1979</option>
                                                                        <option value="1978">1978</option>
                                                                        <option value="1977">1977</option>
                                                                        <option value="1976">1976</option>
                                                                        <option value="1975">1975</option>
                                                                        <option value="1974">1974</option>
                                                                        <option value="1973">1973</option>
                                                                        <option value="1972">1972</option>
                                                                        <option value="1971">1971</option>
                                                                        <option value="1970">1970</option>
                                                                        <option value="1969">1969</option>
                                                                        <option value="1968">1968</option>
                                                                        <option value="1967">1967</option>
                                                                        <option value="1966">1966</option>
                                                                        <option value="1965">1965</option>
                                                                        <option value="1964">1964</option>
                                                                        <option value="1963">1963</option>
                                                                        <option value="1962">1962</option>
                                                                        <option value="1961">1961</option>
                                                                        <option value="1960">1960</option>
                                                                        <option value="1959">1959</option>
                                                                        <option value="1958">1958</option>
                                                                        <option value="1957">1957</option>
                                                                        <option value="1956">1956</option>
                                                                        <option value="1955">1955</option>
                                                                        <option value="1954">1954</option>
                                                                        <option value="1953">1953</option>
                                                                        <option value="1952">1952</option>
                                                                        <option value="1951">1951</option>
                                                                        <option value="1950">1950</option>
                                                                        <option value="1949">1949</option>
                                                                        <option value="1948">1948</option>
                                                                        <option value="1947">1947</option>
                                                                        <option value="1946">1946</option>
                                                                        <option value="1945">1945</option>
                                                                        <option value="1944">1944</option>
                                                                        <option value="1943">1943</option>
                                                                        <option value="1942">1942</option>
                                                                        <option value="1941">1941</option>
                                                                        <option value="1940">1940</option>
                                                                        <option value="1939">1939</option>
                                                                        <option value="1938">1938</option>
                                                                        <option value="1937">1937</option>
                                                                        <option value="1936">1936</option>
                                                                        <option value="1935">1935</option>
                                                                        <option value="1934">1934</option>
                                                                        <option value="1933">1933</option>
                                                                        <option value="1932">1932</option>
                                                                        <option value="1931">1931</option>
                                                                        <option value="1930">1930</option>
                                                                        <option value="1929">1929</option>
                                                                        <option value="1928">1928</option>
                                                                        <option value="1927">1927</option>
                                                                        <option value="1926">1926</option>
                                                                        <option value="1925">1925</option>
                                                                        <option value="1924">1924</option>
                                                                        <option value="1923">1923</option>
                                                                        <option value="1922">1922</option>
                                                                        <option value="1921">1921</option>
                                                                        <option value="1920">1920</option>
                                                                        <option value="1919">1919</option>
                                                                        <option value="1918">1918</option>
                                                                        <option value="1917">1917</option>
                                                                        <option value="1916">1916</option>
                                                                        <option value="1915">1915</option>
                                                                        <option value="1914">1914</option>
                                                                        <option value="1913">1913</option>
                                                                        <option value="1912">1912</option>
                                                                        <option value="1911">1911</option>
                                                                        <option value="1910">1910</option>
                                                                        <option value="1909">1909</option>
                                                                        <option value="1908">1908</option>
                                                                        <option value="1907">1907</option>
                                                                        <option value="1906">1906</option>
                                                                        <option value="1905">1905</option>
                                                                    </CustomInput>
                                                                </FormGroup>
                                                            </Col>
                                                        </Row>
                                                        <Row>
                                                            {this.state.errors[step.id] && !this.state.dateOfBirth &&
                                                            <span
                                                                className={'errors'}>Please select your date of birth</span>
                                                            }
                                                        </Row>
                                                        <Row>
                                                            <Col>
                                                                <div
                                                                    className={'d-flex align-items-center justify-content-center h-100'}>
                                                                    <div className={'d-flex flex-column'}>
                                                                        <Button className={'page-button-back'}
                                                                                onClick={() => this.onStep(previous, step.id, 'prev')}>Back
                                                                        </Button>
                                                                    </div>
                                                                    <div className={'d-flex flex-column'}>
                                                                        <Button className={'page-button'}
                                                                                onClick={() => this.onStep(next, step.id, 'next')}>Next
                                                                        </Button>
                                                                    </div>
                                                                </div>
                                                                {/*</div>*/}
                                                            </Col>
                                                        </Row>
                                                    </div>
                                                )}
                                            />
                                            <Step
                                                id="q4"
                                                render={({next, previous}) => (
                                                    <div>
                                                        <Row>
                                                            <div className={'container h-100'}>
                                                                <div
                                                                    className={'d-flex align-items-center justify-content-center h-100'}>
                                                                    <div className={'d-flex flex-column'}>
                                                                                <span>
                                                                                   <h2 className={'card-text'}>What's your estimated household income?</h2>
                                                                                </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </Row>
                                                        <Row>
                                                            <p className={'suggestion-text'}>Use the slider to
                                                                select
                                                                your range.</p>
                                                        </Row>
                                                        <Row>
                                                            <Col>
                                                                <h3 className={'card-text'}>{this.state.householdIncome}</h3>
                                                                <div className={'slider-container'}>
                                                                    <Slider min={0} max={4} defaultValue={this.state.householdIncomeValue}
                                                                            marks={marks}
                                                                            className={'income-slider'}
                                                                            onChange={(e) => this.onSlide(e, 'householdIncome')}
                                                                    />
                                                                </div>
                                                            </Col>
                                                        </Row>
                                                        <Row>
                                                            {this.state.errors[step.id] && !this.state.householdIncome &&
                                                            <span className={'errors'}>Please select a range</span>
                                                            }
                                                        </Row>
                                                        <br/>
                                                        <Row>
                                                            <Col>
                                                                <div
                                                                    className={'d-flex align-items-center justify-content-center h-100'}>
                                                                    <div className={'d-flex flex-column'}>
                                                                        <Button className={'page-button-back'}
                                                                                onClick={() => this.onStep(previous, step.id, 'prev')}>Back
                                                                        </Button>
                                                                    </div>
                                                                    <div className={'d-flex flex-column'}>
                                                                        <Button className={'page-button'}
                                                                                onClick={() => this.onStep(next, step.id, 'next')}>Next
                                                                        </Button>
                                                                    </div>
                                                                </div>
                                                            </Col>
                                                        </Row>
                                                    </div>
                                                )}/>
                                            <Step
                                                id="q5"
                                                render={({next, previous}) => (
                                                    <div>
                                                        <Row>
                                                            <div className={'container h-100'}>
                                                                <div
                                                                    className={'d-flex align-items-center justify-content-center h-100'}>
                                                                    <div className={'d-flex flex-column'}>
                                                                                <span>
                                                                                   <h2 className={'card-text'}>What is your household size?</h2>
                                                                                </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </Row>
                                                        <Row>
                                                            <Col md={6}>
                                                                <Button
                                                                    className={this.state.householdSize === '1' ? 'answerButtonActive' : 'answerButton'}
                                                                    type="Button"
                                                                    name="householdSize"
                                                                    value={'1'}
                                                                    onClick={(e) => this.onChange(e, next)}
                                                                > 1
                                                                </Button>
                                                            </Col>
                                                            <Col md={6}>
                                                                <Button
                                                                    className={this.state.householdSize === '2' ? 'answerButtonActive' : 'answerButton'}
                                                                    type="Button"
                                                                    name="householdSize"
                                                                    value={'2'}
                                                                    onClick={(e) => this.onChange(e, next)}
                                                                > 2
                                                                </Button>
                                                            </Col>
                                                        </Row>
                                                        <Row>
                                                            <Col md={6}>
                                                                <Button
                                                                    className={this.state.householdSize === '3' ? 'answerButtonActive' : 'answerButton'}
                                                                    type="Button"
                                                                    name="householdSize"
                                                                    value={'3'}
                                                                    onClick={(e) => this.onChange(e, next)}
                                                                > 3
                                                                </Button>
                                                            </Col>
                                                            <Col md={6}>
                                                                <Button
                                                                    className={this.state.householdSize === '4' ? 'answerButtonActive' : 'answerButton'}
                                                                    type="Button"
                                                                    name="householdSize"
                                                                    value={'4'}
                                                                    onClick={(e) => this.onChange(e, next)}
                                                                > 4
                                                                </Button>
                                                            </Col>
                                                        </Row>
                                                        <Row>
                                                            <Col md={6}>
                                                                <Button
                                                                    className={this.state.householdSize === '5' ? 'answerButtonActive' : 'answerButton'}
                                                                    type="Button"
                                                                    name="householdSize"
                                                                    value={'5'}
                                                                    onClick={(e) => this.onChange(e, next)}
                                                                > 5
                                                                </Button>
                                                            </Col>
                                                            <Col md={6}>
                                                                <Button
                                                                    className={this.state.householdSize === '6' ? 'answerButtonActive' : 'answerButton'}
                                                                    type="Button"
                                                                    name="householdSize"
                                                                    value={'6'}
                                                                    onClick={(e) => this.onChange(e, next)}
                                                                > 6
                                                                </Button>
                                                            </Col>
                                                        </Row>
                                                        <Row>
                                                            <Col md={6}>
                                                                <Button
                                                                    className={this.state.householdSize === '7' ? 'answerButtonActive' : 'answerButton'}
                                                                    type="Button"
                                                                    name="householdSize"
                                                                    value={'7'}
                                                                    onClick={(e) => this.onChange(e, next)}
                                                                > 7
                                                                </Button>
                                                            </Col>
                                                            <Col md={6}>
                                                                <Button
                                                                    className={this.state.householdSize === '8+' ? 'answerButtonActive' : 'answerButton'}
                                                                    type="Button"
                                                                    name="householdSize"
                                                                    value={'8+'}
                                                                    onClick={(e) => this.onChange(e, next)}
                                                                > 8+
                                                                </Button>
                                                            </Col>
                                                        </Row>
                                                        <Row>
                                                            {this.state.errors[step.id] && !this.state.householdSize &&
                                                            <span className={'errors'}>Please select the size of your household</span>
                                                            }
                                                        </Row>
                                                        <Row>
                                                            <Col>
                                                                <div
                                                                    className={'d-flex align-items-center justify-content-center h-100'}>
                                                                    <div className={'d-flex flex-column'}>
                                                                        <Button className={'page-button-back'}
                                                                                onClick={() => this.onStep(previous, step.id, 'prev')}>Back
                                                                        </Button>
                                                                    </div>
                                                                    <div className={'d-flex flex-column'}>
                                                                        <Button className={'page-button'}
                                                                                onClick={() => this.onStep(next, step.id, 'next')}>Next
                                                                        </Button>
                                                                    </div>
                                                                </div>
                                                            </Col>
                                                        </Row>
                                                    </div>
                                                )}/>
                                            <Step
                                                id='q6'
                                                render={({next, previous}) => (
                                                    <div>
                                                        <Row>
                                                            <div className={'container h-100'}>
                                                                <div
                                                                    className={'d-flex align-items-center justify-content-center h-100'}>
                                                                    <div className={'d-flex flex-column'}>
                                                                                <span>
                                                                                   <h2 className={'card-text'}>What is your gender?</h2>
                                                                                </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </Row>
                                                        <Row>
                                                        </Row>
                                                        <Row>
                                                            <Col sm="12">
                                                                <div className={"btn-group"}>
                                                                    <Button
                                                                        className={this.state.gender === 'Male' ? 'answerButtonActive' : 'answerButton'}
                                                                        type="Button"
                                                                        name={'gender'}
                                                                        value={'Male'}
                                                                        onClick={(e) => this.onChange(e, next)}
                                                                    >
                                                                        <FontAwesomeIcon
                                                                            className={'button-icons'}
                                                                            icon="male"
                                                                            size={'sm'}
                                                                        />
                                                                        Male
                                                                    </Button>
                                                                    <Button
                                                                        className={this.state.gender === 'Female' ? 'answerButtonActive' : 'answerButton'}
                                                                        type="Button"
                                                                        name={'gender'}
                                                                        value={'Female'}
                                                                        onClick={(e) => this.onChange(e, next)}
                                                                    >
                                                                        <FontAwesomeIcon
                                                                            className={'button-icons'}
                                                                            icon="female"
                                                                            size={'sm'}
                                                                        />
                                                                        Female
                                                                    </Button>
                                                                </div>
                                                            </Col>
                                                        </Row>
                                                        <Row>
                                                            {this.state.errors[step.id] && !this.state.gender &&
                                                            <span
                                                                className={'errors'}>Please select 'male' or 'female'</span>
                                                            }
                                                        </Row>
                                                        <Row>
                                                            <Col>
                                                                <div
                                                                    className={'d-flex align-items-center justify-content-center h-100'}>
                                                                    <div className={'d-flex flex-column'}>
                                                                        <Button className={'page-button-back'}
                                                                                onClick={() => this.onStep(previous, step.id, 'prev')}>Back
                                                                        </Button>
                                                                    </div>
                                                                    <div className={'d-flex flex-column'}>
                                                                        <Button className={'page-button'}
                                                                                onClick={() => this.onStep(next, step.id, 'next')}>Next
                                                                        </Button>
                                                                    </div>
                                                                </div>
                                                            </Col>
                                                        </Row>
                                                    </div>
                                                )}
                                            />
                                            <Step
                                                id="q7"
                                                render={({next, previous}) => (
                                                    <div>
                                                        <Row>
                                                            <Col md={6}>
                                                                <FormGroup>
                                                                    <span>
                                                                        <h2 className={'card-text'}>First Name</h2>
                                                                    </span>
                                                                    <Input name="firstName" id="firstName"
                                                                           onChange={(e) => this.onChange(e)}
                                                                           placeholder="What is your first name?"
                                                                           value={this.state.firstName}
                                                                    />
                                                                    {!this.state.firstName && this.state.errors[step.id] &&
                                                                    <span className={'errors'}>Please enter your first name</span>
                                                                    }

                                                                </FormGroup>

                                                            </Col>
                                                            <Col md={6}>
                                                                <FormGroup>
                                                                     <span>
                                                                        <h2 className={'card-text'}>Last Name</h2>
                                                                    </span>
                                                                    <Input name="lastName"
                                                                           id="lastName"
                                                                           onChange={(e) => this.onChange(e)}
                                                                           placeholder="What is your last name?"
                                                                           value={this.state.lastName}
                                                                    />
                                                                    {!this.state.lastName && this.state.errors[step.id] &&
                                                                    <span className={'errors'}>Please enter your last name</span>
                                                                    }
                                                                </FormGroup>
                                                            </Col>
                                                        </Row>
                                                        <Row>
                                                            <Col>
                                                                <AvForm>
                                                                    <span>
                                                                        <h2 className={'card-text'}>What's your email?</h2>
                                                                    </span>
                                                                    <AvField
                                                                        name="email"
                                                                        id="email"
                                                                        type={'email'}
                                                                        onChange={(e) => this.onChange(e)}
                                                                        placeholder="hi@gmail.com"
                                                                        validate={{email: true}}
                                                                        value={this.state.email}
                                                                        required
                                                                    />
                                                                    {!this.state.email && this.state.errors[step.id] &&
                                                                    <span
                                                                        className={'errors'}>Please enter your email</span>
                                                                    }
                                                                </AvForm>
                                                            </Col>
                                                        </Row>
                                                        <Row>
                                                            <Col>
                                                                <FormGroup>
                                                                     <span>
                                                                        <h2 className={'card-text'}>What's the best number to reach you?</h2>
                                                                    </span>
                                                                    <InputMask className={'form-control'}
                                                                               placeholder={'(###)-###-####'}
                                                                               mask="(999)-999-9999"
                                                                               name="phoneNumber"
                                                                               id="phoneNumber"
                                                                               onChange={(e) => this.onChange(e)}
                                                                               value={this.state.phoneNumber}
                                                                    />
                                                                    {!this.state.phoneNumber && this.state.errors[step.id] &&
                                                                    <span className={'errors'}>Please enter your phone number</span>
                                                                    }
                                                                </FormGroup>
                                                                <small className={'sessionEndSmallText'}>
                                                                    <strong>IMPORTANT</strong>: Please type
                                                                    numbers only. We have a No Spam Guarantee so
                                                                    you will only be contacted once we have a quote
                                                                    that fits your needs.
                                                                </small>
                                                            </Col>
                                                        </Row>
                                                        <Row>
                                                            <Col>
                                                                <FormGroup>
                                                                     <span>
                                                                        <h2 className={'card-text'}>What's the best address to reach you at?</h2>
                                                                    </span>
                                                                    <AddressAutocomplete
                                                                        defaultZipCode={this.state.zipCode}
                                                                        onAddressChange={(e) => this.onChange(e, null)}
                                                                        onZipCodeChange={(e) => this.onChange(e, null)}
                                                                    />
                                                                    {this.state.errors[step.id] && !this.state.phoneNumber &&
                                                                    <span
                                                                        className={'errors'}>Please enter your address</span>
                                                                    }
                                                                </FormGroup>
                                                            </Col>
                                                        </Row>
                                                        <Row>
                                                            <Col xs={12}>
                                                                <div>
                                                                    <Button color={'success'}
                                                                            className={'submit-button'}
                                                                            onClick={(e) => this.onSubmit(e, step.id)}>
                                                                        Submit
                                                                    </Button>
                                                                    <Button
                                                                        className={'last-page-back-button'}
                                                                        onClick={() => this.onStep(previous, step.id, 'prev')}>
                                                                        Back
                                                                    </Button>
                                                                </div>
                                                            </Col>
                                                        </Row>
                                                        <Row>
                                                            <Col>
                                                                <p className={'sessionEndSmallText'}>By clicking
                                                                    the
                                                                    "Submit" Button, you agree by electronic
                                                                    signature to the <a
                                                                        href="https://www.healthinsurancequote.shop/terms-of-service23891796">Terms
                                                                        of Use</a> and <a
                                                                        href="https://www.healthinsurancequote.shop/privacy-policy">Privacy
                                                                        Policy</a>. We respect your privacy. Your
                                                                    information is completely encrypted and 100%
                                                                    secure. You will be matched with up to 5
                                                                    partners including US Health Advisors, to
                                                                    contact you about health insurance via
                                                                    telephone
                                                                    through the use of an autodialer or
                                                                    prerecorded
                                                                    voice, SMS and MMS (data charges may apply),
                                                                    and
                                                                    email even if you are on a corporate, state
                                                                    or
                                                                    national Do Not Call Registry. You do not
                                                                    have
                                                                    to agree to receive such calls or messages
                                                                    as a
                                                                    condition of getting any service or good
                                                                    from us
                                                                    or our partners.it..</p>
                                                            </Col>
                                                        </Row>
                                                    </div>
                                                )}
                                            />
                                        </Steps>
                                    </Form>
                                </div>
                            )}>
                        </Wizard>
                    </Col>
                </Row>
            </Container>
        );
    }
}

const
    mapStateToProps = (state) => {
        return state;
    };

const
    mapDispatchToProps = (dispatch) => {
        return ({
            postForm: (formData) => {
                dispatch(postFormData(formData))
            }
        })
    };

export default connect(mapStateToProps, mapDispatchToProps)(HealthInsuranceForm);
